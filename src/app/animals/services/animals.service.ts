import {Injectable} from '@angular/core';
import {Animals} from '../models/animals.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AnimalsService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Animals[]> {
    return this.httpClient.get<Animals[]>('http://localhost:3000/animals');
  }

}

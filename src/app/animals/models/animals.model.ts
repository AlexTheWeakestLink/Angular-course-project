export class Animals {
  id: number;
  type: string;
  age: number;
  color: string;
  favoriteFood: string;
  imageUrl: string;
}

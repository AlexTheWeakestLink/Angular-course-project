import { Component, OnInit } from '@angular/core';
import {Animals} from './models/animals.model';
import {AnimalsService} from './services/animals.service';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent implements OnInit {

    animals: Animals[];

    constructor(private animalsService: AnimalsService) { }

    ngOnInit() {
      this.animalsService.getAll().subscribe((animals) => {
        this.animals = animals;
      });
    }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdopterComponent} from './adopter/adopter.component';
import { AnimalsComponent } from './animals/animals.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'adopters',
    pathMatch: 'full'
  },
  {
    path: 'adopters',
    component: AdopterComponent
  },
  {
    path: 'animals',
    component: AnimalsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

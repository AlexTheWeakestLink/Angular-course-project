import {Injectable} from '@angular/core';
import {Adopter} from '../models/adopter.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AdopterService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Adopter[]> {
    return this.httpClient.get<Adopter[]>('http://localhost:3000/adopters');
  }

}

import { Component, OnInit } from '@angular/core';
import {Adopter} from './models/adopter.model';
import {AdopterService} from './services/adopter.service';

@Component({
  selector: 'app-adopter',
  templateUrl: './adopter.component.html',
  styleUrls: ['./adopter.component.css']
})
export class AdopterComponent implements OnInit {

  adopters: Adopter[];

  constructor(private adopterService: AdopterService) { }

  ngOnInit() {
    this.adopterService.getAll().subscribe((adopters) => {
      this.adopters = adopters;
    });
  }
}

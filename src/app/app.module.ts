import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {AdopterService} from './adopter/services/adopter.service';
import {HttpClientModule} from '@angular/common/http';
import {AnimalsService} from './animals/services/animals.service';
import { AdopterComponent } from './adopter/adopter.component';
import { AnimalsComponent } from './animals/animals.component';


@NgModule({
  declarations: [
    AppComponent,
    AdopterComponent,
    AnimalsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    AdopterService,
    AnimalsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
